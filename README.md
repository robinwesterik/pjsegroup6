# pjsegroup6

Project Software Engineering Group 6
HAAK has been asked to design, realize and test a weather station, that can retrieve data from sensors such as: temperature, ambient light and/or barometric pressure. The data is transmitted wirelessly and can be retrieved and stored in a database. 
With all of that data HAAK has to design a graphically user interface, where the user can display all the weather data. This project has been commissioned by Saxion Hogeschool. The team of HAAK consists of the following members: Robin Westerik, Matthijs Padding, Ivan Dukart, Ivan Kostroba and Tiemen Slotboom. With all 5 members, HAAK has exactly 7 weeks to deliver a good and working product. The purpose of this project is that we learn to cooperate and work in an agile environment and learn to use tools like scrum, git, and unit tests. 
