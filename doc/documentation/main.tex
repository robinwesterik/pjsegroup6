\documentclass{article}

% Include common used packages
\input{packages.tex}

\acsetup{first-style=short,hyperref=true,tooltip=true}
\DeclareAcroListHeading{section}{\section}

\input{abbreviations.tex}

\title{Documentation \\ \small{Project Software Engineering} \\\vspace{5 mm} \includegraphics[width=\textwidth/4]{resources/images/logoblacktransparent.png}}
\author{
    \begin{tabular}{rl}
        \textbf{Authors:} & \\
        T. Slotboom & \texttt{<464499@student.saxion.nl>} \\
        I. Dukart   & \texttt{<460295@student.saxion.nl>} \\
        I. Kostroba & \texttt{<463378@student.saxion.nl>} \\
        R. Westerik & \texttt{<459170@student.saxion.nl>} \\
        M. Padding  & \texttt{<477841@student.saxion.nl>} \\
        \\
        \textbf{Instructors} & \\
        C.G.M. Slot & \texttt{<c.g.m.slot@saxion.nl>} \\
        R.J.W.T Tangelder & \texttt{<r.j.w.t.tangelder@saxion.nl>} \\
        \\
        \textbf{Customer} & \\
        Saxion & University of Applied Sciences \\
        C.G.M. Slot & \texttt{<c.g.m.slot@saxion.nl>}
    \end{tabular}
}
\date{\today}

\begin{document}
\maketitle
\newpage
\tableofcontents
\newpage

\section{Introduction}
Project Software Engineering Group 6 HAAK has been asked to design, realize and test a weather station, that can retrieve data from sensors such as: temperature, ambient light and/or barometric pressure.
The data is transmitted wirelessly and can be retrieved and stored in a database. With all of that data HAAK has designed a graphically user interface, where the user can display all the weather data.
This project has been commissioned by Saxion Hogeschool. The team of HAAK consists of the following members: Robin Westerik, Matthijs Padding, Ivan Dukart, Ivan Kostroba and Tiemen Slotboom. With all 5 members, HAAK had exactly 7 weeks to deliver a good and working product. The purpose of this project is that we learn to cooperate and work in an agile environment and learn to use tools like scrum, git, and unit tests.
\section{Architectural Overview}
\begin{center}
\vspace{5 mm} \includegraphics[width=\textwidth]{resources/images/connectionoverview.png}
\end{center}
Cloud services are becoming more prevalent in everyday systems than ever before.\cite{CloudTrends}
With this in mind, HAAK designed a weather system that keeps the VPS as the center of attention.\
The \ac{VPS} fetches data that is pushed from \ac{TTN} and stores it in a database. It displays this data through a graphical web interface. This uses a custom built \ac{API} to access the database.
Users can authenticate themselves by logging into the backend to view the \ac{GUI} and change their settings.

\section{Communication between components}

Let's explain the communication between the all the components used to create the weather station. We'll be using a bottom-up approach, beginning with the LoPy.\\

\subsection{The LoPy}
The LoPy is a microcontroller that has \ac{LoRa} functionality built-in, allowing the LoPy to connect to the nearest LoRa gateway. It has a humidity, pressure, temperature, and two lux sensors. 
LoRa uses the 868MHz frequency band to provide long range low power wide area networking. With a gateway, one can link a LoRa device up with LoRaWAN.\cite{LoPy}\\
\ac{LoRaWAN} is a setup in which gateways relays messages between LoRa devices and a centralized network on the internet. This way, one can implement a LoRa setup which has indirect communication with the internet.
The Things Network (TTN) is an open source infrastructure aiming at providing a free LoRaWAN network cover. This project is developed by a growing community across the world, and is based on voluntary contributions to the project.\cite{TTN}
For the project, HAAK is using The Things Network as the infrastructure for communication between our LoRa devices and the internet. This goes as follows:

\begin{enumerate}
\item The LoRa device sends encoded data to the gateway
\item TTN fetches data from the LoRa device using the gateway
\item TTN temporarily saves this data
\item Using MQTT, the VPS fetches data from TTN and put it towards the database (InfluxDB).
\end{enumerate}

\subsection{MQTT Connection}
\ac{MQTT} is a lightweight messaging protocol for small sensors and mobile devices, optimized for high-latency or unreliable networks.\cite{MQTT} One of the goals of this project is to work with MQTT.
HAAK uses MQTT to connect with \ac{TTN} and it's \ac{VPS}. Specifically, HAAK makes use of the InfluxDB Telegraf plugin. To make use of MQTT within Telegraf, one needs to edit it's configuration files to fill in credentials and topics so Telegraf can subscribe to TTN.

\subsection{Virtual Private Server}
For hosting data and providing computing resources, HAAK used a \ac{VPS} from Linode. Because of mail server limitations, the team ended up locally hosting the final product on a Raspberry Pi
The final product has the following installed:

\begin{itemize}
\item InfluxDB with Telegraf (Docker Instance) 8086
\item Chronograf on port 8888
\item Grafana (Docker Instance) on port 3000
\item HAAK Web GUI on port 80
\end{itemize}
In order to protect against accidentally breaking the system or file corruption, The team runs daily backups. HAAK also runs snapshots for whenever it creates a large update to it's system. Updates to the server's custom code are rolled out via git.

\section{Key Concepts}
In this section HAAK will describe some highlights of the code that deserves mentioning. We will discuss the encoding procedure, the working of the timestamps and a piece of the backend that mails the magic link, as well as some unit testing.
\pagebreak
\subsection{Encoding of the sensor values}
In order to configure LoRa for the biggest distance, one needs to minimize the data sent while making sure the sensor data still has accuracy. This can be done by encoding as much data as possible in the least amount of bits. Below we can see an excerpt of our code, describing how we can compress data as much as possible.
\begin{verbatim}
#   Byte 1  |   Byte 2  |   Byte 3  |   Byte 4
# __________|___________|___________|_________
# 1100 100x | xxxx xxxx | xxxx xxxx | xxxx xxxx        Humidity
# xxxx xxx1 | 1001 00xx | xxxx xxxx | xxxx xxxx        Lux
# xxxx xxxx | xxxx xx11 | 1111 1xxx | xxxx xxxx        Pressure
# xxxx xxxx | xxxx xxxx | xxxx x111 | 1111 xxxx        Temperature
# xxxx xxxx | xxxx xxxx | xxxx xxxx | xxxx 1111        Temp Decimal
\end{verbatim}
\subsection{Email Validation}
This part of the code validates the email, generates a challenge, stores that challenge in the cookie session and emails the challenge to the user.
\begin{verbatim}
/// # Arguments
///
/// * `form` - JSON data of the login form, containing user's email
/// * `session` - Session containing all CookieSession data
///
/// # Remarks
///
/// Should only be called from actix_web
pub async fn login_submit(form: Json<Identity>, session: Session) -> HttpResponse {
    let email = form.email.clone();

    if session.get::<String>("email").unwrap().is_some() {
        return HttpResponse::SeeOther()
            .header(actix_web::http::header::LOCATION, "/")
            .finish();
    }

    if !validator::validate_email(email.as_str()) {
        return HttpResponse::UnprocessableEntity().body("Invalid email");
    }

    let challenge = generate_challenge();

    let _ = session.set(
        "pending_login",
        LoginChallenge {
            email: email.clone(),
            challenge: challenge.clone(),
        },
    );

    match email::send_challenge(email, challenge) {
        Ok(_) => HttpResponse::Ok().body("Check your mail for login code"),
        Err(_) => HttpResponse::InternalServerError().body("Could not send authentication mail"),
    }
}
\end{verbatim}

\subsection{Displaying data}
In order to display the latest data on the top of the screen, data must be unpacked from the API. This is how the current values can be displayed.
\begin{verbatim}
  var hoverDetail = new Rickshaw.Graph.HoverDetail( {
    graph: graph,
    xFormatter: function(x) {
      const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
      return new Date(x).toLocaleDateString('nl-NL', options) + " " + new Date(x).toLocaleTimeString();
    },
    formatter : function(series, x, y){
      switch(series.name){
        case "Humidity":
          return series.name + ": " + Math.round(y*100)/100 + "%";
          break;
        case "Lux":
          return series.name + ": " + Math.round(y*100)/100 + "%";
          break;
        case "Temperature":
          return series.name + ": " + Math.round(y*100)/100 + " " + 
                unit_dictionary.temperature[options.temperature];
          break;
        case "Pressure":
          return series.name + ": " + Math.round(y*1000)/1000 + " " + 
                unit_dictionary.pressure[options.pressure];
          break;
      }
      return "ERROR: OOF";
    }
  });
\end{verbatim}

This function handles the updates, this function is called from the different buttons
\begin{verbatim}
function updateGraph(timestamp){
  switch(timestamp){
    case "QuarterYear":
      array_length = 12961;
      break;
    case "Month":
      array_length = 4321;
      break;
    case "Week":
      array_length = 1009;
      break;
    case "Day":
      array_length = 145;
      break;
    case "last":
      array_length = array_length;
      break;
  }
  clearGraph();
  parsedResponse = temp_parsedResponse.slice(temp_parsedResponse.length - array_length,temp_parsedResponse.length);
\end{verbatim}
This code handles every timestamp, with only one variable and that is arraylength. Arraylength has a couple of fixed values for example: one week, one day, one month or 3 months. parsedResponse is then sliced up, and so the latest week/day/month/3months is being displayed in the graph. 
\subsection{Testing the Python Encoding}
Unit Testing has been used throughout the project, one of the great examples of this is our PyTest helpers script where we test if the encoding is in the proper format.
\begin{verbatim}
import pytest
import helpers

@pytest.mark.parametrize(
        "lower,val,upper,expected", [
            (0, 200, 100, 100),
            (100, 0, 200, 100)
        ],
)
def test_clamp(lower, val, upper, expected):
    assert helpers.clamp(lower, val, upper) == expected

@pytest.mark.parametrize(
        "raw,expected", [
            ([0, 0], 1),
            ([65535, 65535], 152283.6795),
            ([100, 0], 177.43),
            ([100, 100], 232.37),
            ([0, 100], 11.85),
        ],
)
def test_raw2lux(raw, expected):
    assert helpers.raw2lux(raw) == expected

@pytest.mark.parametrize(
        "hum,lux,press,temp,dec,expected", [
            (16, 53, 1019, 37, 3, [0x20, 0xD6, 0x7C, 0x33]),
        ],
)
def test_encode(hum, lux, press, temp, dec, expected):
    assert helpers.encode(hum, lux, press, temp, dec) == expected
\end{verbatim}

\pagebreak
\section{Database}
For our database, we're using InfluxDB.
InfluxDB is an open-source time series database developed by InfluxData. It is written in Go and optimized for fast, high-availability storage and retrieval of time series data in fields such as operations monitoring, application metrics, Internet of Things sensor data, and real-time analytics.
A time series database is a system that's optimized for storing and serving time series (like days, weeks, etc.) through pairs of times and values.\\

Because of it's focus on IoT, it's massive adoption, and plugins like Telegraf which let's one easily communicate via MQTT, HAAK has chosen InfluxDB as the platform of choice.
To monitor and edit InfluxDB, Chronograf is installed on the VPS. Chronograf is a graphical web interface (comparable to PhpMyAdmin).
The database structure is generated using telegraf.

\begin{center}
\vspace{5 mm} \includegraphics[width=\textwidth]{resources/images/chronograf.jpeg}
\end{center}

\section{API}
The API is written in Node.js. It's primary purpose is to fetch the data from the InfluxDB Database.
For accessing the InfluxDB, the team made use of the node-influx client.\cite{node-influx}\\

The data is fetched using an express get statement. Express is a widely used, fast web framework for Node.js. 
In this statement we query one influx sql statement, giving all the time point data from the last year.
If the response status is 200, we fetched a result. If the response status is 400, we fetched an error.

HAAK chose to use one query because of security and performance benefits. This avoids the possibility of any SQL injection from the front-end, because of layer abstraction.
While the initial load of all the data point can be taking a bit at first from loading all the data points, the responsiveness during use makes the trade-offs worthwhile.

A client of the API can load and unpack this data and create a graph, using for example Rickshaw, which is based on d3.js. It can also fetch different units, depending on their request set in the backend settings.

\pagebreak

\section{GUI}
For creating the GUI HAAK has chosen a framework in order accelerate creating a modern and flexible interface
The GUI was built using available components from this framework, including buttons, and special grid system that allows creating responsive columns.
HAAK also used a Javascript Toolkit to provide a graphical representation of all data.

\begin{center}
\vspace{5 mm} \includegraphics[width=\textwidth]{resources/images/lightgui.png}
\end{center}

\begin{center}
\vspace{5 mm} \includegraphics[width=\textwidth]{resources/images/darkgui.png}
\end{center}
\pagebreak

\subsection{Layout}
The GUI has a basic design with main content being allocated in the center of the website.
The top of the GUI has a stylish top-bar with a team logo on the left side. Main content 
represents the graph itself and four components displaying current (live) data
for temperature, humidity, pressure and luminosity. To the right of the graph one can
see the legend, that allows one to select which data to display on the graph.\

Furthermore, with the preview slider below a graph one can choose a custom time range of displayed data 
(i.e. shorter period of time). To the left from the main content interface are six 
buttons, which allow one to change the graph or settings for that graph.

\begin{center}
\vspace{5 mm} \includegraphics[width=\textwidth]{resources/images/protogui.png}
\end{center}

\subsection{Framework}
HAAK used Bootstrap, which is an open source framework for developing with HTML, CSS, and JS.\cite{Bootstrap}
Bootstrap has a grid system with responsive containers. Containers are used to contain, 
pad, and position the content within them. Bootstrap also has pre-made components, which are easy to use throughout the layout.

\subsection{Graph}
For displaying an interactive graph, HAAK decided to use Rickshaw.
Rickshaw is a JavaScript toolkit for creating interactive time series graphs.
Since the database was built on a time series database, this seemed a great match.
HAAK decided on having one graph displaying all four data points (temperature, humidity,
pressure and luminosity). This way, we optimize space and it's easy to compare values.
\pagebreak
\section{Backend}
The backend is written in Rust, a safe and concurrent systems programming language. Besides this it also has one of the fastest web frameworks out there (Actix-web) \cite{techempower}.
For storage Redis was chosen for it's fast speed and mature code.\\

The backend handles authentication and per-user settings like selecting units for temperature. Users can be added and removed.
To easily handle front-end the team opted to use Askama for its Jinja templating system, which will help a lot to bind the front-end and back-end together.\cite{askama}

\begin{center}
\vspace{5 mm} \includegraphics[width=\textwidth/2]{resources/images/settings.jpeg}
\end{center}

\subsection{Security}
As an authentication method was needed, the team opted to implement Magic-link authentication. It boils down to storing just an email and sending a verification email to login. This way the server does not store any significant data (such as passwords) and thereby circumvents the need of high security. All authentication links are one-time use and the expiration date is short (2 hours). Registration is done in the same way, an admin account can send a registration email to a client.

\begin{center}
\vspace{5 mm} \includegraphics[width=\textwidth/2]{resources/images/login.jpeg}
\end{center}

\begin{center}
\vspace{5 mm} \includegraphics[width=\textwidth/2]{resources/images/email.jpeg}
\end{center}

\section{Conclusion}
For more information about the project, please visit the cited works in the bibliography and the source code in the git repository.
An installation guide is also available below.

\newpage
\printbibliography
\newpage
\printacronyms[include-classes=abbrev,name=Abbreviations]
\end{document}
