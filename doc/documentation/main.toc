\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\contentsline {section}{\numberline {1}Introduction}{3}{section.1}% 
\contentsline {section}{\numberline {2}Architectural Overview}{3}{section.2}% 
\contentsline {section}{\numberline {3}Communication between components}{3}{section.3}% 
\contentsline {subsection}{\numberline {3.1}The LoPy}{4}{subsection.3.1}% 
\contentsline {subsection}{\numberline {3.2}MQTT Connection}{4}{subsection.3.2}% 
\contentsline {subsection}{\numberline {3.3}Virtual Private Server}{4}{subsection.3.3}% 
\contentsline {section}{\numberline {4}Key Concepts}{4}{section.4}% 
\contentsline {subsection}{\numberline {4.1}Encoding of the sensor values}{5}{subsection.4.1}% 
\contentsline {subsection}{\numberline {4.2}Email Validation}{5}{subsection.4.2}% 
\contentsline {subsection}{\numberline {4.3}Displaying data}{6}{subsection.4.3}% 
\contentsline {subsection}{\numberline {4.4}Testing the Python Encoding}{7}{subsection.4.4}% 
\contentsline {section}{\numberline {5}Database}{8}{section.5}% 
\contentsline {section}{\numberline {6}API}{8}{section.6}% 
\contentsline {section}{\numberline {7}GUI}{9}{section.7}% 
\contentsline {subsection}{\numberline {7.1}Layout}{10}{subsection.7.1}% 
\contentsline {subsection}{\numberline {7.2}Framework}{10}{subsection.7.2}% 
\contentsline {subsection}{\numberline {7.3}Graph}{10}{subsection.7.3}% 
\contentsline {section}{\numberline {8}Backend}{11}{section.8}% 
\contentsline {subsection}{\numberline {8.1}Security}{11}{subsection.8.1}% 
\contentsline {section}{\numberline {9}Conclusion}{11}{section.9}% 
