# Installation guide
An installation guide for Debian (and to a certain extent Ubuntu) is provided. The application can be installed on most other linux distros, the only difference being the install command and package names.
# Variables
Export these variables before running any other command.
`WEATHER_USER` is used to create an admin account and `WEATHER_URL` is used to send emails to users (from weather@WEATHER_URL and linking to WEATHER_URL in the emails).
```
export WEATHER_USER=<your email>
export WEATHER_URL=<webserver URL>
export WEATHER_IP=<webserver internal IP>
```
for example
```
export WEATHER_USER=john.doe@gmail.com
export WEATHER_EMAIL=johndoe.com
export WEATHER_IP=192.168.1.203
```

# Certificates
Both the api and the backend are designed to run on HTTPS and thus need certificates. Put `key.pem` and `cert.pem` in the root of both of the API folder (`pjsegroup6/src/API`) and the Backend folder (`pjsegroup6/src/Server`).

# Ports
There are 2 ports in use for this application.
Open port `443` for the back-end server and `3010` for the api.

# Debian
## API
The api runs on NodeJS with express as web server. Run the following commands to install nodejs, npm (package manager) and the libraries required to run the api.
```
sudo apt-get install nodejs npm
npm install --save influx express cors
```
To then run the api run the following command
```
node app.js
```

Another way of running is:
```
nohup node app.js &
```
This starts the api in the background, persisting even if the terminal closes. Do however run with `node app.js` first to check if the api starts (and does not error because of AddressInUse or other errors).

## Database
### InfluxDB
InfluxDB is not in the debian repos. To install it run the following commands to add it to the repo, update our repo list and install it.
```
wget -qO- https://repos.influxdata.com/influxdb.key | sudo apt-key add -
echo "deb https://repos.influxdata.com/debian buster stable" | sudo tee /etc/apt/sources.list.d/influxdb.list
sudo apt update
sudo apt install influxdb
```

If installation fails run
`sudo systemctl unmask influxdb`
and then try running `sudo apt install influxdb` again

To start the database for the current session and on boot run the following commands.
```
sudo systemctl unmask influxdb
sudo systemctl enable influxdb
sudo systemctl start influxdb
```

### Telegraf
Telegraf is used to get data from TTN via MQTT to the database. To install it run:
```
sudo apt-get install telegraf
```

Open `/etc/telegraf/telegraf.conf` with your favourite editor (do not forget root access) and add the following code (at the end of the file), changing the server, username and password to the ones matching on TTN.
Username is the name of the application and password is the access key (that you can generate by going to the application -> settings -> access keys)
```
[[inputs.mqtt_consumer]]
  servers = ["tcp://eu.thethings.network:1883"]
  qos = 0
  connection_timeout = "30s"
  topics = ["+/devices/+/up"]
  client_id = "ttn"
  username = "psjegroup6"
  password = "ttn-account-v2.4sDiDRVl05FOI1KmR7jSYt-qDJ9gk-6-1AdBCGSNswc"
  data_format = "json"
```
We also need to designate a table to store our data in. Scroll to the top until you find `# database = "telegraf"` in the `[[outputs.influxdb]]` section and uncomment this line and change the database to `"sensors"`.

Then restart telegraf
```
sudo systemctl start telegraf
```

## Backend
### Email sending
run
```
sudo apt install mailutils
sudo apt install postfix
```
to install mailutils and postfix.
When postfix is installed it opens a ncurses configuration screen in the terminal. Press enter to go to the next screen.
On the screen of mail configuration the option `Internet site` is preselected. Press enter to select that option.
On the last screen you need to input the system email name. Enter your domain name (top level only).

To configure postfix to only send emails from this server open `/etc/postfix/main.cf` in your favourite editor (make sure to use root or sudo) such as:
```
sudo vim /etc/postfix/main.cf
```
Change the line `inet_interfaces = all` to `inet_interfaces = loopback-only`.

In the `mydestination = ` line include your domain name.
The line will look like
```
mydestination = $myhostname, mppadding.dev, ...
```

Finally restart postfix using
```
sudo systemctl restart postfix
```

### Redis
To install redis run
```
sudo apt install redis
```
Then check if redis is running by running the following command
```
redis-cli ping
```
And check if it returns PONG. If it is not running run `systemctl start redis`. Running with root if that is required.

To create the initial admin user run
```
redis-cli mset user:$WEATHER_EMAIL "admin" settings:$WEATHER_EMAIL\:timeframe "Week" settings:$WEATHER_EMAIL\:theme "Light" settings:$WEATHER_EMAIL\:units:pressure "Bar" settings:$WEATHER_EMAIL\:units:temperature "Celsius"
```
This will register an admin user with email `$WEATHER_EMAIL` (see Variables section). With a default timeframe of one week, bar as unit of pressure and celsius as unit of temperature. Values can be changed in the command to
```
Temperature -> [Celsius, Fahrenheit, Kelvin]
Pressure -> [Atmosphere, Millibar, Bar, PSI, Mercury]
```
However, settings can also be changed after starting the server.

### Server
The git repo currently provides 2 pre-compiled binaries. One for x86-64 LSB and one for 32-bit LSB ARM (compiled for Raspberry Pi Model 3B - armv7l)
If there does not exist a pre-compiled binary for your platform go to [rustup](https://rustup.rs/) and follow the instructions there to install the rust compiler.
Afterwards go into the server source folder (`pjsegroup6/src/Server`) and run `cargo build --release` (note: on lesser machines this command might take a while to run. A complete rebuild on the raspberry pi model 3B took approximately 73 minutes).

The compiled binary can then be found in `pjsegroup6/src/Server/target/release`.
To run the binary we first need to set some environment variables, run the following commands
```
export COOKIE_SECRET_KEY=`cat /dev/urandom | head -c 32 | base64`
export WEATHER_PORT=443
```
The first command generates a new secret key to encrypt the cookies with and the second command dictates what port to use.

The application can then be started by running the correct binary in `target/release/server/`, such as:
```
nohup sudo -E target/release/server &
```
This starts the server in the background (not linked to the current terminal session) and provides the current environment variables to the binary.
