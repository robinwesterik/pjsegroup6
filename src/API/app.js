//API
const Influx = require('influx');
const express = require('express');
const path = require('path');
const os = require('os');
const bodyParser = require('body-parser');
const app = express();
const influx = new Influx.InfluxDB('http://127.0.0.1:8086/sensors');

var https = require('https');
var fs = require('fs');
var cors = require('cors');

//Set port to 3010
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.set('port', 3010);

var hskey = fs.readFileSync('key.pem');
var hscert = fs.readFileSync('cert.pem');

var options = {
  key: hskey,
  cert: hscert
};

//Influx measurements done
influx.getMeasurements() 
  .then(names => console.log('Measurement names: ' + names.join(', '))) //Display 
  .then(() => {
	var server = https.createServer(options, app);
	server.listen(app.get('port'), () => {
      		console.log(`Listening on ${app.get('port')}.`); //Give port
    	});
  })
  .catch(error => console.log({ error }));

app.get('/api/v1/usage', (request, response) => { //Express get statement
	var tempDiv = 1;
	var tempAdd = 0;
	var pressDiv = 1000;
	var frameDiv = 1;

  switch(request.query.temperature) 
{
  case "Kelvin": // Conversion from Celsius to Kelvin
    tempAdd=273.15;
    tempDiv=1;
    break;
  case "Fahrenheit": // Conversion from Celsius to Fahrenheit
    tempAdd=32;
    tempDiv=0.55555555555;
    break;
  default:
    // do nothing (celsius)
} 

  switch(request.query.pressure) 
{
  case "Atmosphere": // Conversion from MBar to Atmosphere
    pressDiv=1013.25;
    break;
  case "PSI":
    pressDiv=68.9475728;
    break;
  case "Millibar": // Conversion from MilliBar to Millibar
    pressDiv=1;
    break;
  case "Mercury": // Conversion from MilliBar to Mercury
    pressDiv=33.863886666667;
  default:
    // do nothing (mbar/1000)
} 

  influx.query(`
    SELECT mean("payload_fields_humidity") AS "humidity", mean("payload_fields_lux") AS "lux", mean("payload_fields_temperature")/` + tempDiv.toString() + `+` + tempAdd.toString() + ` AS "temperature", mean("payload_fields_pressure")/` + pressDiv.toString() + ` AS "pressure" 
    from mqtt_consumer
    where time > now() - 90d
    group by time(10m)
    `)
    .then(result => response.status(200).json(result))
    .catch(error => response.status(500).json({ error }));
});

