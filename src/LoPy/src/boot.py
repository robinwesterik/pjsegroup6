###
# boot.py
#
# This file is responsible for booting the FiPy/LoPy, setting up UART
# and running main.py
#
# Author: Matthijs Padding <mppadding@protonmail.com>
# Date: November 30th, 2019
# Last Edit: November 30th, 2019
###

"""
Responsible for booting the FiPy/LoPy, setting up UART for communications
and running main.py
"""

import os
from machine import UART
import machine

UART = UART(0, baudrate=115200)
os.dupterm(UART)
print()         # Empty line, since dupterm doesnt send a terminating line

machine.main('main.py')
