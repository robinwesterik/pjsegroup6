#
# connection.py
#
#
#
# Author: Matthijs Padding <mppadding@protonmail.com>
# Date: December 6th, 2019
# Last Edit: December 6th, 2019
#

# Python
import binascii
import socket
import time

# LoRa
from network import LoRa                    # Lora connectivity

# Application
from config import config                   # Config files
from LogLevel import LogLevel               # LogLevel enum

lora = LoRa(mode=LoRa.LORAWAN)

app_eui = binascii.unhexlify(config["ttn"]["app_eui"])
app_key = binascii.unhexlify(config["ttn"]["app_key"])


def connect():
    lora.join(activation=LoRa.OTAA, auth=(app_eui, app_key), timeout=0)

    join_wait = 0
    while True:
        time.sleep(config["ttn"]["connect_interval"] / 5)
        if not lora.has_joined():
            join_wait += 1
            if join_wait == 5:
                lora.join(
                    activation=LoRa.OTAA,
                    auth=(app_eui, app_key),
                    timeout=0
                )

                if config["log_level"] >= LogLevel.INFO:
                    print("Trying rejoin")

                join_wait = 0
        else:
            break

    if config["log_level"] >= LogLevel.INFO:
        print("Connected")

    # Safety measure to ensure we don't start sending packets too soon
    time.sleep(2)


def send(payload):
    # create a LoRa socket
    s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

    # set the LoRaWAN data rate
    s.setsockopt(socket.SOL_LORA, socket.SO_DR, 5)

    # make the socket non-blocking
    s.setblocking(False)

    if config["log_level"] >= LogLevel.DEBUG:
        print("Sending payload " + str(binascii.hexlify(payload)))

    # send the payload
    s.send(payload)
