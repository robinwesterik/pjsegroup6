###
# Helpers.py
#
# Contains helper functions that don't fit in a specific file.
#
# Author: Matthijs Padding <mppadding@protonmail.com>
# Date: December 6th, 2019
# Last Edit: December 6th, 2019
###

"""Contains helper functions that don't fit in a specific file."""


def clamp(minimum, value, maximum):
    """Clamps value between minimum and maximum"""
    return max(minimum, min(value, maximum))


# Calculates one channel lux from 2 channel results.
# Uses the formula found in Appendix A of the LTR329ALS01 datasheet
def raw2lux(raw):
    """
    Calculates one channel lux from 2 channel results.
    Uses the formula found in Appendix A of the LTR329ALS01 datasheet
    Found at
    https://forum.arduino.cc/index.php?action=dlattach;topic=563507.0;attach=322476
    """
    ch0 = raw[0]
    ch1 = raw[1]

    # Prevents divide by zero
    if ch0 + ch1 == 0:
        return 1

    ratio = ch1 / (ch0 + ch1)

    if ratio < 0.45:
        return 1.7743 * ch0 + 1.1059 * ch1

    if ratio < 0.64:
        return 4.2785 * ch0 - 1.9548 * ch1

    return 0.5926 * ch0 + 0.1185 * ch1


# Encode values into byte array
def encode(humidity, lux, pressure, temperature, temperature_decimal):
    """Encodes values into a byte array to be send with LoRa"""
    # We add offsets here to map it to 0-127
    humidity = clamp(0, humidity, 100)
    lux = clamp(0, lux, 100)
    pressure = clamp(0, pressure - 940, 127)
    temperature = clamp(0, temperature + 30, 127)

    #   Byte 1  |   Byte 2  |   Byte 3  |   Byte 4              | Key
    # ----------|-----------|-----------|-----------------------|----------
    # 1100 100x | xxxx xxxx | xxxx xxxx | xxxx xxxx  		    | Humidity
    # xxxx xxx1 | 1001 00xx | xxxx xxxx | xxxx xxxx 		    | Lux
    # xxxx xxxx | xxxx xx11 | 1111 1xxx | xxxx xxxx 		    | Pressure
    # xxxx xxxx | xxxx xxxx | xxxx x111 | 1111 xxxx 		    | Temperature
    # xxxx xxxx | xxxx xxxx | xxxx xxxx | xxxx 1111             | Temp Decimal
    #
    #    Bits   |  Max Val  | Step size | Min Range | Max Range | Key
    # ----------------------------------------------------------|---------
    #     7     |    100    |      1    |     0     |    100    | Humidity
    #     7     |    100    |      1    |     0     |    100    | Lux
    #     7     |    127    |      1    |   940     |   1067    | Pressure
    #     7     |    127    |      1    |   -30     |     97    | Temperature
    #     4     |      1    |   0.0625  |     0     | 0.9375    | Temp Decimal
    raw = [
        (humidity << 1) | (lux >> 6),
        ((lux << 2) & 0xFF) | (pressure >> 5),
        ((pressure & 0x1F) << 3) | (temperature >> 4),
        (temperature & 0xF) << 4 | temperature_decimal
    ]

    return raw
