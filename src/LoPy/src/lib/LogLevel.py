# Since MicroPython does not support enums (and python supports it since 3.4)
# this function will emulate enums (to a certain extent)


def enum(**enums):
    return type('Enum', (), enums)


LogLevel = enum(
    NONE=0,       # No messages
    INFO=1,       # Only connect/rejoin messages
    DEBUG=2,      # Also includes payloads
    SENSORS=3     # Also prints sensors values
)
