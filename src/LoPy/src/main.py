#
# Main.py
#
# Main python file used for uploading to the LoPy/FiPy
# This handles all sensor data, formats it, encodes it and
# sends it to TTN over LoRa.
#
# Author: Matthijs Padding <mppadding@protonmail.com>
# Date: November 30th, 2019
# Last Edit: December 6th, 2019
#

# Python
import time

# Application
from config import config                   # Config files
import sensors
import helpers
import connection

# Connect to LoRa
connection.connect()

while True:
    # Measure and encode payload
    payload = bytes(helpers.encode(*sensors.measure()))

    # Send payload to TTN
    connection.send(payload)

    # Sleep before sending next package
    time.sleep(config["ttn"]["send_interval"])
