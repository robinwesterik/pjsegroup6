#
# Sensors.py
#
"""
Handles sensor measuring of the light, humidity, temperature
and pressure sensors
"""
#
# Author: Matthijs Padding <mppadding@protonmail.com>
# Date: December 6th, 2019
# Last Edit: December 6th, 2019

# Python
import math

# Sensors
from pysense import Pysense                 # Pysense board
from SI7006A20 import SI7006A20             # Humidity and temperature sensor
from LTR329ALS01 import LTR329ALS01         # Light sensor
from MPL3115A2 import MPL3115A2, PRESSURE   # Pressure sensor

# Application
from config import config                   # Config files
from LogLevel import LogLevel               # LogLevel enum
from helpers import raw2lux                 # Raw 2 channel to 1 channel lux

# Setup Pysense
PYSENSE = Pysense()
SENSOR_PRESSURE = MPL3115A2(PYSENSE, mode=PRESSURE)
SENSOR_HUMID = SI7006A20(PYSENSE)
SENSOR_LIGHT = LTR329ALS01(PYSENSE)


def measure():
    """
    Measure sensors, return a tuple containing humidity, lux, pressure
    temperature and temperature decimal
    """

    avg_tmp = (SENSOR_PRESSURE.temperature() + SENSOR_HUMID.temperature()) / 2
    tmp = math.floor(avg_tmp)

    # We need to map the decimal part of the temperature to unsigned
    # integers
    dec = round((avg_tmp - tmp) / 0.0625)

    # If we round it to 1, add one to the tmp and set decimal to 0
    if dec == 1:
        tmp += 1
        dec = 0

    press = round(SENSOR_PRESSURE.pressure() / 100)
    hum = round(SENSOR_HUMID.humidity())

    # Map lux logarithmically
    lux = round(math.log((raw2lux(SENSOR_LIGHT.light()))) / math.log(10) * 20)

    if config["log_level"] >= LogLevel.SENSORS:
        print("=== Measurement ===")
        print("Temperature:\t" + str(avg_tmp))
        print("Pressure:\t\t" + str(press) + " mbar")
        print("Humidity:\t\t" + str(hum) + "%")
        print("Illuminance:\t" + str(lux) + "%")

    return (hum, lux, press, tmp, dec)
