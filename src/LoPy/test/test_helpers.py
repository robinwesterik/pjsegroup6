import pytest

import helpers


@pytest.mark.parametrize(
        "lower,val,upper,expected", [
            (0, 200, 100, 100),
            (100, 0, 200, 100)
        ],
)
def test_clamp(lower, val, upper, expected):
    assert helpers.clamp(lower, val, upper) == expected


@pytest.mark.parametrize(
        "raw,expected", [
            ([0, 0], 1),
            ([65535, 65535], 152283.6795),
            ([100, 0], 177.43),
            ([100, 100], 232.37),
            ([0, 100], 11.85),
        ],
)
def test_raw2lux(raw, expected):
    assert helpers.raw2lux(raw) == expected


@pytest.mark.parametrize(
        "hum,lux,press,temp,dec,expected", [
            (16, 53, 1019, 37, 3, [0x20, 0xD6, 0x7C, 0x33]),
        ],
)
def test_encode(hum, lux, press, temp, dec, expected):
    assert helpers.encode(hum, lux, press, temp, dec) == expected
