# SRC
Contains source code for all applications, sorted in folders.

## API
Contains the code that runs the api server.

## LoPy
Contains all files that are to be uploaded to the LoPy.

## Server
Contains the code of the backend server

## TTN
Contains the decoder that is going to be used in TTN
