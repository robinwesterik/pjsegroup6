function Decoder(bytes, port) {
  var decoded = {};

  // Decode from data to obj
  decoded.humidity    = bytes[0] >> 1;
  decoded.lux         = ((bytes[0] & 1) << 6)   | (bytes[1] >> 2);
  decoded.pressure    = ((bytes[1] & 0x3) << 5) | (bytes[2] >> 3);
  decoded.temperature = ((bytes[2] & 0x7) << 4) | (bytes[3] >> 4);
  decoded.temperature += (bytes[3] & 0xF) * 0.0625;

  // Add/remove offsets
  decoded.pressure    += 940;
  decoded.temperature -= 30;

  return decoded;
}
