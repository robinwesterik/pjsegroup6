import os
import sys
from py_mini_racer import py_mini_racer
ctx = py_mini_racer.MiniRacer()

with open('decode.js', 'r') as myfile:
    ctx.eval(myfile.read())


def test(args, out):
    result = ctx.call("Decoder", args)

    if result != out:
        sys.exit(os.EX_DATAERR)


test([0x20, 0xCC, 0xF4, 0x2D], {'humidity': 16, 'lux': 51, 'pressure': 970, 'temperature': 36.8125})
test([0x1E, 0xCC, 0xF4, 0x35], {'humidity': 15, 'lux': 51, 'pressure': 970, 'temperature': 37.3125})
test([0x20, 0xCC, 0xF4, 0x40], {'humidity': 16, 'lux': 51, 'pressure': 970, 'temperature': 38})
